const mongoose = require("mongoose");

const UserSchema = new mongoose.Schema({
  email: {
    type: String,
    unique: true,
  },
  password: {
    type: String,
    unique: true,
  },
});

module.exports = mongoose.models.Users || mongoose.model("Users", UserSchema);
