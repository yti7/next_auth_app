import dbConnect from "../../utils/dbConnect";
import User from "../../models/User";

import bcrypt from "bcryptjs";

dbConnect();

export default async function (req, res) {
  const { method } = req;
  const { email, password } = req.body;
  switch (method) {
    case "POST":
      if (!email || !password) {
        return res.status(400).json({ success: false, msg: "FAF" });
      }
      try {
        const userExists = await User.findOne({ email });
        if (userExists)
          return res.status(400).json({ success: false, msg: "UAE" });
        const hash = await bcrypt.hash(password, 4);
        const newUser = await User.create({ email, password: hash });
        res.status(201).json({ success: true, msg: "USR", newUser });
      } catch (error) {
        return res.status(400).json({ success: false, msg: error });
      }
      break;
    default:
      res.status(400).json({ success: false, msg: "SWW" });
  }
}
