import mongoose from "mongoose";

const connection = {};

const dbConnect = async () => {
  if (connection.isConnected) {
    return;
  }
  const db = await mongoose.connect(process.env.MONGO_URI, {
    useNewUrlParser: true,
  });
  connection.isConnected = true;
  console.log("[+] DB Connected");
};

export default dbConnect;
