/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  env: {
    MONGO_URI: "mongodb://admin:7247@nextie-shard-00-00.jkhre.mongodb.net:27017,nextie-shard-00-01.jkhre.mongodb.net:27017,nextie-shard-00-02.jkhre.mongodb.net:27017/?ssl=true&replicaSet=atlas-q4a6ys-shard-0&authSource=admin&retryWrites=true&w=majority"
  }
}

module.exports = nextConfig
